from django.db import models
from django.contrib.auth.models import User

class Movie(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    release_date = models.DateField()
    director = models.CharField(max_length=100)
    created_by = models.ForeignKey(User, related_name='movies', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Series(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    release_date = models.DateField()
    seasons = models.IntegerField()
    created_by = models.ForeignKey(User, related_name='series', on_delete=models.CASCADE)

    def __str__(self):
        return self.title
